import Vue from 'vue';
import VueRouter from 'vue-router';
//ici on import tous nos components
import Home from '../components/Home.vue';
import Opening_1 from '../components/Opening_1.vue';
import Opening_2 from '../components/Opening_2.vue';
import Opening_3 from '../components/Opening_3.vue';
import Opening_4 from '../components/Opening_4.vue';
import Opening_5 from '../components/Opening_5.vue';

Vue.use(VueRouter)

//Vue.component('Favoris', Favoris);

//ici on crée nos routes vers les autres pages components
const routes = [
  { path: '/', component: Home}, //localhost:8080/   PAGE D'ACCUEIL
  { path: '/opening1', component: Opening_1},
  { path: '/opening2', component: Opening_2},
  { path: '/opening3', component: Opening_3},
  { path: '/opening4', component: Opening_4},
  { path: '/opening5', component: Opening_5}
];

//on exporte par défault le router
export const router = new VueRouter({
  routes: routes
});